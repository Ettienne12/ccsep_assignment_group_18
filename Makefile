# A sample Makefile to use make command to build, test and run the program
# Guide: https://philpep.org/blog/a-makefile-for-your-dockerfiles/
#APP=isec3004.assignment

all: run

run:
	php -S localhost:9000 -t src/ 1>/dev/null 2>&1 &

clean:
	mv usersdatabase/users/data/1.json 1.json
	rm -rf usersdatabase
	mkdir usersdatabase
	mkdir usersdatabase/users
	mkdir usersdatabase/users/data
	mv 1.json usersdatabase/users/data/
