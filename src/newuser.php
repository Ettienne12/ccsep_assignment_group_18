<?php
    session_start();
    error_reporting(0);
    require_once "../SleekDB-master/src/Store.php";
    use SleekDB\Store;

    $databaseDirectory = "../usersdatabase";
    $usersStore = new Store("users", $databaseDirectory);

    $username = $_POST["username"];
    $secret = $_POST["secret"];

    $credentials = [
        "username" => $username,
        "secret" => $secret
    ];

    $currUser = $usersStore->findOneBy(
            ["username", "=", $username]
    );
    if(empty($currUser))
    {
        $usersStore->insert($credentials);
        $_SESSION["signup"] = "Account created successfully.";
        $_SESSION["log"] = "";
    } else {
        $_SESSION["signup"] = "Username already exists.";
    }

    header("Location: signup.php");
?>
