<?php
session_start();
if(empty($_SESSION["user"]))
{
    $_SESSION["user"] = "None";
}
?>
<html>
<head>
    <style>
        body {
            text-align: center;
            margin-top: 100px;
        }
        body * {
            margin: 10px;
        }
    </style>
    <title>CCSEP Assignment Group 18</title>
</head>
<body>
    <h1>Private Lounge</h1>
    <p>For users with accounts only.</p>
    <?php echo "Welcome, ". $_SESSION["user"]. "."; ?>
    <form action="logout.php" method="post">
        <input type="submit" value="logout">
    </form>
</body>
</html>
